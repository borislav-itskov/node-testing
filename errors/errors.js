exports.handleFormErrors = function(err, res, req) {
    if (err.name != 'ValidationError') {
        return;
    }

    let errors = {}
    Object.values(err.errors).forEach(el => {
        errors[el.path] = el.message
    })
    backURL = req.header('Referer') || '/';
    req.flash('errMsg', errors)
    res.status(400).redirect(backURL)
};