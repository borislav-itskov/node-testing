var greet = require('./module-example')
var Person = require('./class-example')
var Emiiter = require('./emitter')

// bobby = new Person('Bobsy', 'Dopsy')
// bobby.greet()

emitter = new Emiiter()
emitter.on('greet', function() {
    console.log('some hello from somewhere')
})
emitter.on('greet', function() {
    console.log('Another greet')
})

emitter.emit('greet')

// template literals

var name = 'John Doe'
var greet = `Hello ${name}`
console.log(greet)