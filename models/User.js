//Require Mongoose
var mongoose = require('mongoose');

// Define schema
var Schema = mongoose.Schema;

var UserSchema = new Schema({
  first_name: {
    type: String,
    min: 1,
    max: 255,
    required: true
  },
  last_name: {
    type: String,
    min: 1,
    max: 255,
    required: true
  },
  username: {
    type: String,
    min: 1,
    max: 64,
    required: true
  }
});

// Compile model from schema
module.exports = mongoose.model('User', UserSchema );