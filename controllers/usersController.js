var User = require('../models/User')
var formErrors = require('../errors/errors')

exports.list = function(req, res) {
    let errors = req.flash('errMsg')
    errors = errors.length !== 0 ? errors[0] : []
    res.render('users/all', {
        title: 'All users',
        errors: errors
    });
};

exports.single = async function(req, res) {
    user = await User.findById(req.params.id).exec().catch(err => res.sendStatus(404))
    if (! user._id) return;

    res.render('users/single', {
        title: 'A single user',
        id: req.params.id,
        user: user
    })
};

exports.create = function(req, res) {
    var user = new User( req.body );
    user.save()
        .then(user => res.redirect('/users/' + user.id))
        .catch((err) => formErrors.handleFormErrors(err, res, req))
};

exports.flash = function(req, res) {
    req.flash('errors', 'Flash is back!')
    res.redirect('/users');
};