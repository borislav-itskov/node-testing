var express = require('express');
var controller = require('../controllers/usersController');
var router = express.Router();

/* GET users listing. */
router.get('/', controller.list);
router.get('/flash', controller.flash);
router.get('/:id', controller.single);
router.post('/', controller.create);

module.exports = router;
