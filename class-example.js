function Person(firstname, lastname) {
    this.firstname = firstname
    this.lastname = lastname
}

Person.prototype.greet = function() {
    console.log(this.firstname + ' ' + this.lastname)
}

module.exports = Person